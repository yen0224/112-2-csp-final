# 112-2-CSP-final

## Introduction

This is a simple application for the integration with Google Task.<br/>
However, in order to make sure we can submit the project on time, this Project will be separated into two parts.<br/>

- Part 1 (Milestone1): The basic function of the application, **with single user and local DB only**
- Part 2 (Milestone2): The advanced function of the application, **with support to fetch data from Google Task API**

## Getting started

The first thing you need to do is to clone the repository:

```bash
git clone https://gitlab.com/yen0224/112-2-csp-final.git
```

If your IDE is IntelliJ IDEA, all dependencies should be ready to use, you can open the project by clicking `File` -> `Open` -> `112-2-csp-final`.<br/>
Then, you can run the application by clicking the green triangle on the left side of the main function.

If you are using other IDEs, YOU SHOULD MAKE SURE YOU HAVE THE FOLLOWING DEPENDENCIES AND CONFIGURATIONS HAS BEEN SET UP:
* JDBC Driver: locate at `lib/sqlite-jdbc 3.45.3.0.jar`, you should add it to your project as a library
* JUnit: for testing, though we have not written any test cases yet, you should have it in your project
* slf4j: for logging

NOTE: You have not thing to do to set up the DB, the driver should create one in your file system.<br>
NOTE2: Same as the config file, the application will create one when not found.

## Milestone1
### Project Structure
```
(root)
|-- src
|   |-- main
|       |-- java
|       |   |-- ncu/final_projects
|       |   |   |-- Main.java
|       |   |   |-- Database.java
|       |   |   |-- Configuration.java
|       |   |  
|       |   |-- module-info.java
|       |
|       |-- resources/ncu/final_projects
|
|-- lib
|
|-- (others)
```



