module ncu.final_project {
    requires javafx.controls;
    requires javafx.fxml;

    requires java.logging;
    requires java.xml;
    requires java.sql;
    requires jdk.httpserver;

    requires org.xerial.sqlitejdbc;
    requires google.api.client;
    requires com.google.api.services.tasks;
    requires com.google.api.client.extensions.java6.auth;
    requires com.google.api.client.extensions.jetty.auth;
    requires com.google.api.client.auth;
    requires com.google.api.client;
    requires com.google.api.client.json.gson;
    requires org.kordamp.ikonli.javafx;
    requires com.google.common;


    opens ncu.final_project to javafx.fxml;
    exports ncu.final_project;
    exports ncu.final_project.Utils;
    opens ncu.final_project.Utils to javafx.fxml;
    exports ncu.final_project.Controllers;
    opens ncu.final_project.Controllers to javafx.fxml;
    exports ncu.final_project.Templates;
    opens ncu.final_project.Templates to javafx.fxml;
}