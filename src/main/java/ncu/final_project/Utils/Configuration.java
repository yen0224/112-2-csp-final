package ncu.final_project.Utils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Configuration class
 * This class is used to read/write the configuration file with given path, and store the configurations in a map.
 * Methods:
 * 1. get(String key): return the value of the key in the map
 * 2. set(String key, Object value): set the value of the key in the map
 * 3. save(String configPath): save the configurations to the file with the given path
 * 4. read(String configPath): read the configurations from the file with the given path
 * 5. Configuration(): constructor
 * 6. Configuration(Map<String, Object> source): constructor with parameters
 */

public class Configuration {
    //private mapping
    private Map<String, Object> configs;
    //constructor
    public Configuration() {
        configs = new HashMap<>();
    }
    //constructor with parameters
    public Configuration(Map<String, Object> source) {
        configs = source;
    }
    //get method
    public String get(String key) {
        return (String) configs.get(key);
    }
    //set method
    public void set(String key, Object value) {
        configs.put(key, value);
    }
    //save file
    public void save(String configPath) throws IOException {
        File cfg = new File(configPath);
        //write to file
        FileInputStream fis = null;
        Writer writer = new FileWriter(cfg);
        writer.write(configs.toString());
        //close
        writer.close();
    }
    //read file
    public void read(String configPath) throws IOException {
        File cfg = new File(configPath);
        //read from file
        FileInputStream fis = new FileInputStream(cfg);
        //save to configs
        byte[] data = new byte[(int) cfg.length()];
        fis.read(data);
        String str = new String(data);
        //remove {}
        str = str.substring(1, str.length() - 1);
        //parse
        String[] pairs = str.split(", ");
        for (String pair : pairs) {
            String[] keyValue = pair.split("=");
            configs.put(keyValue[0], keyValue[1]);
            //System.out.println(keyValue[0] + " " + keyValue[1]);
        }
        //close
        fis.close();
    }
}
