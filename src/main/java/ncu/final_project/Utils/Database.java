package ncu.final_project.Utils;

import ncu.final_project.Templates.*;

import java.io.IOException;
import java.sql.*;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.*;

import com.google.api.services.tasks.model.TaskList;

import static ncu.final_project.Main.logger;

/**
 * <p>
 *     <b>Database class</b>
 * </p>
 *
 * <p>This class is used to interact with the database.</p>
 * <p>Schema of the database: There are 2 tables in the database, one for lists recording and one for tasks</p>
 * <ul>
 *     <li>
 *        <b>Lists table</b>: id, taskListID, name, source
 *     </li>
 *     <li>
 *         <b>Tasks table</b>: id, uuid, name, descriptions, priority, status, taskListID, taskListName dueDate, createDate, lastModified
 *     </li>
 * </ul>
 *
 */

public class Database{

    private TaskClient taskClient; //init
    private Connection conn; //init

    public Database(String dbPath) throws SQLException{
        //connect to db
        try {
            //load driver
            Class.forName("org.sqlite.JDBC");
            System.setProperty("jdbc.drivers", "org.sqlite.JDBC");
            //connect to db
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
            //test if the table exists, if not, it will create one
            if(!checkTable()){
                //create table of lists
                executeUpdate("CREATE TABLE IF NOT EXISTS lists (id INTEGER PRIMARY KEY AUTOINCREMENT, taskListID TEXT, name TEXT, source TEXT, sync INTEGER)");
                addDefaultList();
                //create table of todos
                executeUpdate("CREATE TABLE IF NOT EXISTS tasks (id INTEGER PRIMARY KEY AUTOINCREMENT, uuid TEXT, name TEXT, descriptions TEXT, priority INTEGER, status TEXT, source TEXT ,taskListID TEXT, taskListName TEXT, dueDate TEXT, createDate TEXT, lastModified TEXT)");
                // for avoid the db is empty, also for instructions
                addTask("This is an example", "Example Task", 0, "0", "0000", "General", "2021-01-01");
            }else{
                return;
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void setTaskClient(TaskClient taskClient){
        this.taskClient = taskClient;
    }

    /** !!!NOTICE!!! This function is only be used to creating tables, other operations should use prepareStatement, in order to avoid SQL injection
     * Execute an update query
     * @param query the query to be executed
     */
    public void executeUpdate(String query) throws SQLException {
        try {
            conn.createStatement().executeUpdate(query);
        } catch (SQLException e) {
            throw new SQLException("Error executing query");
        }
    }

    /** Task CRUD functions */
    // Create local task
    public void createTask(String name, String description, int priority, String listID, String DueDate) throws IOException, SQLException {
        //judge if the list to add is a google list
        if(isRemoteList(listID)){
            //if it is, call the taskClient to create the task
            RemoteTaskClass task = new RemoteTaskClass(UUID.randomUUID().toString(), name, description, priority, "0", listID, fetchListInfo(listID).getName(), DueDate, null, null);
            TaskClass r = taskClient.createTask(task, task.getListID());
            //add the task to the db
            addTask(r.getUuid(), r.getName(), r.getDescriptions(), r.getPriority(), r.getStatus(), r.getListID(), fetchListInfo(listID).getName(), r.getDueDate(), r.getCreatedDate(), r.getLastModified());
        }else{
            //if not, add the task to the db
            addTask(name, description, priority, "0", listID, fetchListInfo(listID).getName(), DueDate);
        }
    }

    public void addTask(String name, String descriptions, int priority, String status, String ListID, String ListName, String dueDate) throws SQLException {

        UUID uuid = UUID.randomUUID(); //uuid generate when creating

        long currentTime = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(currentTime);

        PreparedStatement ps = conn.prepareStatement("INSERT INTO tasks (uuid, name, descriptions, priority, status, source, taskListName, taskListID, dueDate, createDate, lastModified) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        ps.setString(1, uuid.toString());
        ps.setString(2, name);
        ps.setString(3, descriptions);
        ps.setInt(4, priority);
        ps.setString(5, status);
        ps.setString(6, "local"); //local task (not from google
        ps.setString(7, ListName);
        ps.setString(8, ListID);
        ps.setString(9, dueDate);
        ps.setString(10, date);
        ps.setString(11, date);
        ps.executeUpdate();
    }

    // create task record from Google task
    public void addTask(String externalID, String name, String descriptions, int priority, String status, String ListID, String ListName, String dueDate, String cDate, String lDate) throws SQLException {
        //System.out.println("list Name"+ListName+"list ID"+ListID);
        PreparedStatement ps = conn.prepareStatement("INSERT INTO tasks (uuid, name, descriptions, priority, status, source, taskListID, taskListName, dueDate, createDate, lastModified) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        ps.setString(1, externalID);
        ps.setString(2, name);
        ps.setString(3, descriptions);
        ps.setInt(4, priority);
        ps.setString(5, status);
        ps.setString(6, "google");
        ps.setString(7, ListID);
        ps.setString(8, ListName);
        ps.setString(9, dueDate);
        ps.setString(10, cDate);
        ps.setString(11, lDate);
        ps.executeUpdate();
    }

    /**
     * @param ListName: the name of the list to be added
     * @return true for success, false for fail (name already exists)
     */
    public boolean addList(String ListName){
        List<String> currentList = fetchListName();
        if(currentList.contains(ListName)){
            return false;
        }
        UUID uuid = UUID.randomUUID();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("INSERT INTO lists (taskListID, name, source) VALUES (?, ?, ?)");
            ps.setString(1, uuid.toString());
            ps.setString(2, ListName);
            ps.setString(3, "local");
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public void addDefaultList(){
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("INSERT INTO lists (taskListID, name, source) VALUES (?, ?, ?)");
            ps.setString(1,"0000");
            ps.setString(2, "General");
            ps.setString(3, "local");
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // for google task importing
    public void addList(String ListName, String ListID){
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("INSERT INTO lists (taskListID, name, source, sync) VALUES (?, ?, ?, ?)");
            ps.setString(1, ListID);
            ps.setString(2, ListName);
            ps.setString(3, "google");
            ps.setInt(4, 1);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<String> fetchListName(){
        String sql = "SELECT name FROM lists";
        List<String> ret = new ArrayList<String>();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ret.add(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }

    /** Read (fetch all)*/
    public TaskClass[] fetchAllTasks() throws SQLException {
        ResultSet rs = conn.createStatement().executeQuery("SELECT * FROM tasks");
        return rs2arrayT(rs);
    }

    public Map<String, String> fetchAllLists(){
        String sql = "SELECT taskListID, name FROM lists";
        Map<String, String> ret = new HashMap<>();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ret.put(rs.getString("name"), rs.getString("taskListID"));
            }
        } catch (SQLException e) {
            logger.info("Error fetching lists");
        }
        return ret;
    }

    public List<TaskListClass> fetchSyncLists(){
        //select from db, where source is "google" and skip is not 1 (skip might be null)
        String sql = "SELECT taskListID, name FROM lists WHERE source = 'google' AND sync = 1";
        List<TaskListClass> ret = new ArrayList<>();
        try {
            int i = 0;
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                i++;
                ret.add(new TaskListClass(rs.getString("taskListID"), rs.getString("name")));
            }
            logger.info(i+" lists to sync");
        } catch (SQLException e) {
            logger.severe(e.getMessage());
            //logger.info("Error fetching lists");
        }
        return ret;
    }

    // Update (all fields), call from client syncing
    private void updateTask(String uuid, String name, String descriptions, int priority, String status, String ListID, String ListName, String dueDate, String lastModified) throws SQLException {
        long currentTime = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(currentTime);

        PreparedStatement ps = conn.prepareStatement("UPDATE tasks SET name = ?, descriptions = ?, priority = ?, status = ?, taskListID = ?, taskListName = ?, dueDate = ?, lastModified=?  WHERE uuid = ?");
        ps.setString(1, name);
        ps.setString(2, descriptions);
        ps.setInt(3, priority);
        ps.setString(4, status);
        ps.setString(5, ListID);
        ps.setString(6, ListName);
        ps.setString(7, dueDate);
        ps.setString(8, lastModified);
        ps.setString(9, uuid);
        ps.executeUpdate();
    }

    // call from local
    private void updateTask(TaskClass task){
        long currentTime = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(currentTime);

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("UPDATE tasks SET name = ?, descriptions = ?, priority = ?, status = ?, taskListID = ?, taskListName = ?, dueDate = ?, lastModified=?  WHERE uuid = ?");
            ps.setString(1, task.getName());
            ps.setString(2, task.getDescriptions());
            ps.setInt(3, task.getPriority());
            ps.setString(4, task.getStatus());
            ps.setString(5, task.getListID());
            ps.setString(6, task.getListName());
            ps.setString(7, task.getDueDate());
            ps.setString(8, date);
            ps.setString(9, task.getUuid());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * <p><b>exchange list handling:</b></p>
     *
     * <p>there are four possibly cases: </p>
     * <ol>
     *     <li>
     *         a task move from local list to google list, which means a task creating of google list is needed,
     *         not only a creation API is call, but also a callback record in the db is needed
     *     </li>
     *     <li>
     *         a task move from google list to local list, which means a task deleting of local list is needed,
     *         db will need to create a uuid for this task, then change the source to local
     *     </li>
     *     <li> local to local, simply update the column in the db</li>
     *     <li> google to google, perform an exchange method of google API</li>
     *
     * </ol>
     **/
    public void updateTask(TaskClass task, String fromListID, String toListID) throws IOException, SQLException {
        //fetch two list information
        TaskListInfoClass fromList = fetchListInfo(fromListID);
        TaskListInfoClass toList = fetchListInfo(toListID);

        //case 1: local to google
        if(fromList.getSource().equals("local") && toList.getSource().equals("google")){
            //create a task in google
            TaskClass created =  taskClient.createTask(task, toList.getId());
            //find new task's list's name with its id
            //first update the column
            updateTask(task.getUuid(), created.getName(), created.getDescriptions(), created.getPriority(), created.getStatus(), created.getListID(), toList.getName(), created.getDueDate(), created.getLastModified());
            //then update uuid
            changeTaskUUID(task.getUuid(), created.getUuid());
            //then update source
            changeTaskSource(created.getUuid(), "google");

        } else if(fromList.getSource().equals("google") && toList.getSource().equals("local")){
            //case 2: google to locally
            //delete the task in local
            taskClient.deleteTask(task.getUuid(), fromListID);
            //update the task
            updateTask(task.getUuid(), task.getName(), task.getDescriptions(), task.getPriority(), task.getStatus(), toListID, toList.getName(), task.getDueDate(), task.getLastModified());
            //change the source
            changeTaskSource(task.getUuid(), "local");

        } else if(fromList.getSource().equals("local") && toList.getSource().equals("local")){
            //case 3: local to local
            //update the task
            updateTask(task);
        } else if(fromList.getSource().equals("google") && toList.getSource().equals("google")){
            //case 4: google to google
            //exchange the task in google
            TaskClass moved = taskClient.moveTask(task, fromListID, toListID);
            //update the task
            updateTask(task.getUuid(), moved.getName(), moved.getDescriptions(), moved.getPriority(), moved.getStatus(), moved.getListID(), toList.getName(), moved.getDueDate(), moved.getLastModified());
            //update the uuid
            changeTaskUUID(task.getUuid(), moved.getUuid());
        }
    }

    private void changeTaskUUID(String oldUUID, String newUUID) {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("UPDATE tasks SET uuid = ? WHERE uuid = ?");
            ps.setString(1, newUUID);
            ps.setString(2, oldUUID);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void changeTaskSource(String id, String to){
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("UPDATE tasks SET source = ? WHERE uuid = ?");
            ps.setString(1, to);
            ps.setString(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public TaskListInfoClass fetchListInfo(String id){
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("SELECT * FROM lists WHERE taskListID = ?");
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return new TaskListInfoClass(rs.getString("taskListID"), rs.getString("name"), rs.getString("source"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
    }
        return null;
    }


    // Update (status field)
    public void fullfillTask(String uuid) throws SQLException {
        PreparedStatement ps = conn.prepareStatement("UPDATE tasks SET status = '1' WHERE uuid = ?");
        ps.setString(1, uuid.toString());
    }

    public void unfullfillTask (String uuid) throws SQLException {
        PreparedStatement ps = conn.prepareStatement("UPDATE tasks SET status = '0' WHERE uuid = ?");
        ps.setString(1, uuid.toString());
    }


    // Delete
    public int deleteTask(String uuid) throws SQLException {
        PreparedStatement ps = conn.prepareStatement("DELETE FROM todos WHERE uuid = ?");
        ps.setString(1, uuid.toString());
        return ps.executeUpdate();
    }

    //check Table
    public boolean checkTable() throws SQLException {
        ResultSet rs = conn.createStatement().executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='tasks'");
        if(rs.next()){
            return true;
        }else{
            return false;
        }
    }

    /**
     * ResourceSet to array Transformer
     * A tool function to convert a ResultSet to a 2D array
     * @param rs the ResultSet to be converted
     * @return the 2D array
     */
    public TaskClass[] rs2arrayT(ResultSet rs) throws SQLException {
        TaskClass ret[] = null;
        while (rs.next()) {
            if (ret == null) {
                ret = new TaskClass[1];
            } else {
                TaskClass temp[] = new TaskClass[ret.length + 1];
                System.arraycopy(ret, 0, temp, 0, ret.length);
                ret = temp;
            }

            if(rs.getString("source").equals("local")){
                ret[ret.length - 1] = new LocalTaskClass(rs.getString("uuid"), rs.getString("name"), rs.getString("descriptions"), rs.getInt("priority"), rs.getString("status"), rs.getString("taskListID"), rs.getString("taskListName"), rs.getString("dueDate"), rs.getString("createDate"), rs.getString("lastModified"));
            }else{
                ret[ret.length - 1] = new RemoteTaskClass(rs.getString("uuid"), rs.getString("name"), rs.getString("descriptions"), rs.getInt("priority"), rs.getString("status"), rs.getString("taskListID"), rs.getString("taskListName"), rs.getString("dueDate"), rs.getString("createDate"), rs.getString("lastModified"));
            }
        }
        return ret;
    }

    public boolean isRemoteList(String ID){
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("SELECT source FROM lists WHERE taskListID = ?");
            ps.setString(1, ID);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                if(rs.getString("source").equals("google")){
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    // this function is called from the TaskClient to handle the response from Google
    public void TaskListSyncHandler(List<TaskList> TLs){

        if(taskClient == null){
            logger.severe("TaskClient is not set before calling");
            return;
        }

        Map <String, String> localLists = fetchAllLists();
        Map <String, String> NewLists = new HashMap<>();

        //System.out.println(localLists);

        // compare local list with remote list
        for(TaskList TL : TLs){
            if(localLists.containsKey(TL.getTitle())){
                // list exists
                // check if the list is modified
                if(localLists.get(TL.getTitle()).equals(TL.getId())){
                    // list is not modified
                    logger.info("List: " + TL.getTitle() + " is not modified");
                }else{
                    // list is modified
                    // update the list
                    PreparedStatement ps = null;
                    try {
                        ps = conn.prepareStatement("UPDATE lists SET name = ? WHERE taskListID = ?");
                        ps.setString(1, TL.getTitle());
                        ps.setString(2, TL.getId());
                        ps.executeUpdate();
                        logger.info("List: " + TL.getTitle() + " is modified, updated in local db");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }else{
                // list does not exist
                // add the list
                //TODO: let user decide whether to sync
                addList(TL.getTitle(), TL.getId()); //db operation, for recording
            }
            //NewLists.put(TL.getId(), TL.getTitle()); //TODO this hashmap is used to ask user whether to sync
        }
        //fetch from db, where source is google and skip is not 1
        logger.info("Sending Syncing request, current time: " + (new Date(System.currentTimeMillis())).toString());
        List<TaskListClass> TaskListToSync = fetchSyncLists();
        //System.out.println(TaskListToSync.size());
        logger.info("fetch from db done, current time: " + (new Date(System.currentTimeMillis())).toString());
        taskClient.syncTask(TaskListToSync);

    }

    public void SyncingToDB(List<TaskListClass> TaskListToSync){
        for(TaskListClass TL : TaskListToSync){
            TaskSyncHandler(TL);
        }
    }

    // call when user delete a list
    public void removeTaskList(String id){
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("DELETE FROM lists WHERE taskListID = ?");
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * This function is called from TaskClient to sync the tasks from Google Task to local database
     * @param TL the TaskListTemplate object that contains the tasks from Google Task
     * */
    public void TaskSyncHandler(TaskListClass TL){
        for( TaskClass task : TL.Tasks){
            try {
                PreparedStatement ps = conn.prepareStatement("SELECT * FROM tasks WHERE uuid = ?");
                ps.setString(1, task.getUuid());
                ResultSet rs = ps.executeQuery();
                if(rs.next()){
                    // task exists
                    // check if the task is modified
                    if(rs.getString("lastModified").equals(task.getLastModified())){
                        // task is not modified
                        continue;
                    }else{
                        // task is modified
                        updateTask(task.getUuid(), task.getName(), task.getDescriptions(), task.getPriority(), task.getStatus(), task.getListID(), task.getListName(), task.getDueDate(), task.getLastModified());
                    }
                }else{
                    // task does not exist
                    addTask(task.getUuid(), task.getName(), task.getDescriptions(), task.getPriority(), task.getStatus(), task.getListID(), task.getListName(), task.getDueDate(), task.getCreatedDate(), task.getLastModified());
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
