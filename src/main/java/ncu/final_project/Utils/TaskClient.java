package ncu.final_project.Utils;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponseException;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;

import com.google.api.services.tasks.Tasks;
import com.google.api.services.tasks.TasksScopes;
import com.google.api.services.tasks.model.Task;
import com.google.api.services.tasks.model.TaskList;
import com.google.api.services.tasks.model.TaskLists;
import ncu.final_project.Templates.RemoteTaskClass;
import ncu.final_project.Templates.TaskListClass;
import ncu.final_project.Templates.TaskClass;

import java.io.*;
import java.security.GeneralSecurityException;
import java.sql.Ref;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static ncu.final_project.Main.logger;

public class TaskClient{

    private final String APPLICATION_NAME = "Task Manager";
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private static NetHttpTransport HTTP_TRANSPORT = null;

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(TasksScopes.TASKS);
    private static final String CREDENTIALS_FILE_PATH = "/ncu/final_project/credentials.json";
    private static InputStream key = null;
    private Tasks service = null;

    private Database db = null;

    /**
     * Creates an authorized Credential object.
     *
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT, final InputStream key)
            throws IOException {

        if (key == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(key));

        //System.out.println(clientSecrets);
        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    public TaskClient(InputStream key, Database dbPath) throws IOException, GeneralSecurityException {
        db = dbPath;
        this.key = key;
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        service = new Tasks.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT, key))
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    // trigger manual sync
    public void Sync(){

    }

    // trigger sync on startup
    public void startUpSync() throws IOException, GeneralSecurityException {
        List<TaskList> taskLists = syncList();
        if (taskLists != null) {
            db.TaskListSyncHandler(taskLists);
        }
    }

    public List<TaskList> syncList() throws IOException, GeneralSecurityException {
        try{
        TaskLists result = service.tasklists().list().execute();
        List<TaskList> taskLists = result.getItems();
        if (taskLists == null || taskLists.isEmpty()) {
            return null;
        } else {
            return taskLists;
        }}catch (TokenResponseException e){
            //token expired
            if(e.getStatusCode() == 400){
                logger.severe("\n\nToke expired, please restart the application.\n\n");
                logout();
            }
        }
        return null;
    }

    public void syncTask(List<TaskListClass> ret) {
        logger.info("Syncing tasks from Google to local database.");
        //System.out.println(ret.isEmpty());
        try {
            for(TaskListClass TL: ret){
                logger.info("Syncing List:" + TL.listName + " (" + TL.listUuid + ").");
                List<Task> tasks = null;
                try {
                    tasks = service.tasks().list(TL.listUuid).execute().getItems();
                }catch (GoogleJsonResponseException e){
                    // handle for different status code
                    if(e.getStatusCode() == 404){
                        // means the tasklist is not found (deleted)
                        // remove it from the database
                        db.removeTaskList(TL.listUuid);
                        logger.info("List:" + TL.listName + " (" + TL.listUuid + ") not found. Removed from database.");
                    }
                }catch (TokenResponseException e){
                    //token expired
                    if(e.getStatusCode() == 400){
                        logger.severe("\n\nToke expired, please restart the application.\n\n");
                        logout();
                    }
                }
                if (tasks == null || tasks.isEmpty()) {
                    logger.info("Synced List:" + TL.listName + " (" + TL.listUuid + "). But no tasks found.");
                } else {
                    //System.out.println("Tasks:");
                    for (Task task : tasks) {
                        //System.out.printf("%s (%s)\n", task.getTitle(), task.getId());
                        String status ="";
                        if(task.getStatus().equals("completed")){
                            status = "1";
                        }else{
                            status = "0";
                        }
                        TaskClass newTask = new RemoteTaskClass(task.getId(), task.getTitle(), task.getNotes(), 0, status, TL.listUuid, TL.listName, task.getDue(), "imported from Google", task.getUpdated());
                        TL.addTask(newTask);
                    }
                    logger.info("List:" + TL.listName + " (" + TL.listUuid + ") synced successfully.");
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return;
        }
        db.SyncingToDB(ret);
    }

    // create a new task in given listid
    public TaskClass createTask(TaskClass taskToAdd, String listId) throws IOException {
        Task task = new Task();
        task.setTitle(taskToAdd.getName());
        task.setNotes(taskToAdd.getDescriptions());
        String due = taskToAdd.getDueDate();
        // let this part compatible with RFC format
        if(due != null){
            due+="T00:00:00.000Z";
            task.setDue(due);
        }
        Task created = service.tasks().insert(listId, task).execute();
        return new RemoteTaskClass(created.getId(), created.getTitle(), created.getNotes(), 0, "0", listId, "", created.getDue(), "imported from Google", created.getUpdated());
    }

    public void completeTask(String taskId, String listId) throws IOException {
        //System.out.println("Completing task: " + taskId + " in list: " + listId);
        Task task = service.tasks().get(listId, taskId).execute();
        task.setStatus("completed");
        service.tasks().update(listId, taskId, task).execute();
    }

    public void uncompleteTask(String taskId, String listId) throws IOException {
        //System.out.println("uncompleting task: " + taskId + " in list: " + listId);
        Task task = service.tasks().get(listId, taskId).execute();
        task.setStatus("needsAction");
        service.tasks().update(listId, taskId, task).execute();
    }

    public void deleteTask(String taskId, String listId) throws IOException {
        service.tasks().delete(listId, taskId).execute();
    }

    public TaskClass moveTask(TaskClass task, String fromListid, String toListId) throws IOException {
        Task taskToMove = service.tasks().get(fromListid, task.getUuid()).execute();
        Task ret = service.tasks().insert(toListId, taskToMove).execute();
        service.tasks().delete(fromListid, task.getUuid()).execute();
        return new RemoteTaskClass(ret.getId(), ret.getTitle(), ret.getNotes(), 0, "0", toListId, "", ret.getDue(), "imported from Google", ret.getUpdated());
    }

    public void logout() {
        try {
            File tokens = new File(TOKENS_DIRECTORY_PATH);
            if (tokens.exists()) {
                File[] files = tokens.listFiles();
                if (files != null) {
                    for (File file : files) {
                        file.delete();
                    }
                }
                tokens.delete();
            }
            service = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}