package ncu.final_project.Templates;

import ncu.final_project.Main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

public class RemoteTaskClass extends TaskClass {

    public RemoteTaskClass(String uuid, String name, String descriptions, int priority, String status, String ListID, String ListName, String dueDate, String createdDate, String lastModified) {
        super(uuid, name, descriptions, priority, status, ListID, ListName, dueDate, createdDate, lastModified);
        source = "remote";
        //System.out.println("RemoteTaskClass created: " + name+ " "+ uuid + " in list:"+ ListID);
        //System.out.println(getListID());
    }

    @Override
    public String getDueDate() {
        String ret;
        //input format: YYYY-MM-DDT00:00:00.000Z
        //output format: YYYY-MM-DD
        if(dueDate != null && dueDate.length() >= 10){
            ret = dueDate.substring(0, 10);
            return ret;
        }else{
            return null;
        }
    }

    @Override
    public void setDueDate(String dueDate) {
        //this.dueDate = dueDate+ "T00:00:00.000Z";
        this.dueDate = dueDate;
    }

    @Override
    public void unfinishedTask() throws SQLException, IOException {
        System.out.println(getUuid()+" "+getListID());
        Main.db.unfullfillTask(getUuid());
        Main.taskClient.uncompleteTask(getUuid(), getListID());
        setStatus("0");
    }

    @Override
    public void finishTask() throws SQLException, IOException {
        Main.db.fullfillTask(getUuid());
        System.out.println(getListName());
        Main.taskClient.completeTask(getUuid(), getListID());
        setStatus("1");
    }


}
