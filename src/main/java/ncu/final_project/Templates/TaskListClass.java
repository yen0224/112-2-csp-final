package ncu.final_project.Templates;

import java.util.ArrayList;

public class TaskListClass {
    public String listUuid;
    public String listName;

    public ArrayList<TaskClass> Tasks;

    public TaskListClass(String listUuid, String listName) {
        this.listUuid = listUuid;
        this.listName = listName;
        Tasks = new ArrayList<TaskClass>();
    }

    public void addTask(TaskClass task) {
        Tasks.add(task);
    }
}
