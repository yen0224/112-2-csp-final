package ncu.final_project.Templates;

import java.io.IOException;
import java.sql.SQLException;

public abstract class TaskClass {
    private final String uuid;
    private String name;
    private String descriptions;
    private int priority;
    protected String status; // set to protected to allow child to access
    private String ListName;
    private String ListID;
    protected String dueDate;
    private final String createdDate;
    private String lastModified;
    protected String source;

    public TaskClass(String uuid, String name, String descriptions, int priority, String status, String ListID, String ListName, String dueDate, String createdDate, String lastModified) {
        this.uuid = uuid;
        this.name = name;
        this.descriptions = descriptions;
        this.priority = priority;
        this.status = status;
        this.ListName = ListName;
        this.ListID = ListID;
        this.dueDate = dueDate;
        this.createdDate = createdDate;
        this.lastModified = lastModified;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getListName() {
        return ListName;
    }

    public void setListName(String listName) {
        ListName = listName;
    }

    public String getListID() {
        return ListID;
    }

    public void setListID(String listID) {
        ListID = listID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified() {
        //set to current time
        this.lastModified = String.valueOf(System.currentTimeMillis());
    }

    public abstract String getDueDate();
    public abstract void setDueDate(String dueDate);

    public abstract void unfinishedTask() throws SQLException, IOException;
    public abstract void finishTask() throws SQLException, IOException;
}
