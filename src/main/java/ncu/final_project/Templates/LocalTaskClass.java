package ncu.final_project.Templates;

import ncu.final_project.Main;

import java.sql.SQLException;

public class LocalTaskClass extends TaskClass {

    public LocalTaskClass(String uuid, String name, String descriptions, int priority, String status, String ListID, String ListName, String dueDate, String createdDate, String lastModified) {
        super(uuid, name, descriptions, priority, status, ListID, ListName, dueDate, createdDate, lastModified);
        source = "local";
    }

    @Override
    public String getDueDate() {
        return dueDate;
    }

    @Override
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public void unfinishedTask() throws SQLException {
        Main.db.unfullfillTask(getUuid());
        super.status = "0";
    }

    @Override
    public void finishTask() throws SQLException {
        Main.db.fullfillTask(getUuid());
        super.status = "1";
    }

}
