package ncu.final_project;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import ncu.final_project.Utils.Configuration;
import ncu.final_project.Utils.Database;
import ncu.final_project.Utils.TaskClient;

import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.*;

import java.io.File;
import java.io.IOException;

public class Main extends Application {
    // utility classes
    public static Database db = null;
    public static TaskClient taskClient = null;
    // load key
    private static final InputStream key = Main.class.getResourceAsStream("credentials.json");
    // logger
    public static Logger logger = Logger.getLogger(Main.class.getName());
    public static boolean loginStatus = false;

    public static int preferredWidth;
    public static int preferredHeight;
    public static String defaultListID = "0000";

    @Override
    public void start(Stage stage) throws IOException, GeneralSecurityException {

        /*-----------------CONFIGURATION-----------------*/
        /* This part of the code is used to read the configuration file and apply the values to the application,
        * if the file is not found, it will create one with default values
        */

        preferredWidth = 960;
        preferredHeight = 640;
        //path to db
        String dbPath = "db.sqlite";


        File cfg = new File(".config");
        if (!cfg.exists()) {
            //logging-info
            logger.info("config not found, creating one with default values");
            //create config.xml with default values
            Map<String, Object> source = new HashMap<>();
            source.put("preferredWidth", preferredWidth);
            source.put("preferredHeight", preferredHeight);
            source.put("dbPath", dbPath);
            source.put("defaultListID", defaultListID);

            Configuration cfgToWrite = new Configuration(source);
            try {
                cfgToWrite.save(".config");
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            //logging-info
            logger.info("config.xml found, reading values");
            //for development, print the values
            Configuration cfgToRead = new Configuration();
            try {
                cfgToRead.read(".config");
            } catch (IOException e) {
                e.printStackTrace();
            }
            //set the values
            preferredWidth = Integer.parseInt(cfgToRead.get("preferredWidth"));
            preferredHeight = Integer.parseInt(cfgToRead.get("preferredHeight"));
            defaultListID = cfgToRead.get("defaultListID");
            dbPath = cfgToRead.get("dbPath");
        }

        /*-----------------DATABASE-----------------*/
        /* This part of the code is used to connect to the database, if the database file is not found, it will create one.
        * Also, if the user has changed the path to the database, it will connect to the new path.
        */

        try{
            //creat db as a thread
            db = new Database(dbPath);
            logger.info("Connected to database");
        }catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("FATAL ERROR");
            alert.setHeaderText("FATAL ERROR");
            alert.setContentText("Error connecting to database, might be due to the database file is damaged or the driver is lost\nExiting...");
            //logging-error
            logger.severe("Error connecting to database");
            e.printStackTrace();
            System.exit(1);
        }

        // check if user already login by checking the token
        File testToken = new File("tokens");
        if (testToken.exists()) {
            taskClient = new TaskClient(key, db);
            db.setTaskClient(taskClient);
            loginStatus = true;
        }


        /*-----------------UI-----------------*/
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("views/MainPage.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), preferredWidth, preferredHeight);
        stage.setTitle("Task Application");
        stage.setScene(scene);
        stage.show();

        stage.setOnCloseRequest(e -> {
            //logging-info
            logger.info("closing application");
        });

    }

    public static void createClient() {
        try {
            taskClient = new TaskClient(key, db);
            loginStatus = true;
            db.setTaskClient(taskClient);
            taskClient.startUpSync();
            logger.info("Connected to Google Task API");
        } catch (IOException | GeneralSecurityException e) {
            e.printStackTrace();
        }
    }

    public static void TaskSync() throws IOException, GeneralSecurityException {
        if(taskClient != null){
            taskClient.startUpSync();
        }
    }

    public static void Sync(){
        if(taskClient != null){
            taskClient.Sync();
        }
    }

    public static void destructClient() {
        taskClient = null;
        logger.info("Disconnected from Google Task API");
    }

    public static void logout() {
        taskClient.logout();
        destructClient();
        logger.info("Logged out from Google Task API");
    }


    public static void main(String[] args) {
        launch();
    }
}