package ncu.final_project.Controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ncu.final_project.Main;
import ncu.final_project.Templates.TaskClass;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;

/**
 * <p>
 *     <b>MainPageController</b>
 * </p>
 *
 * <p>TODOS</p>
 * */

public class MainPageController {
    // global variables
    public int currentTaskIndex = -1; // mark which task is selected
    protected TaskClass[] Tasks = null; // store all tasks from db
    protected static Map<String, String> ListNameIDMap = null; // store all list names and their corresponding IDs
    protected static Map<String, String> reverseListNameIDMap = null; // store all list names and their corresponding IDs
    private ArrayList<Card> cards = new ArrayList<>(); // store all cards

    // Task Card Template
    class Card extends StackPane {
        TaskClass task;
        Rectangle card;
        public Card(TaskClass task, int index) {
            //System.out.println("Card created: " + task.getName() + " " + task.getUuid() + " in list:" + task.getListID() + " " + task.getListName() + " " + task.getDueDate() + " " + task.getPriority() + " " + task.getStatus() + " " + task.getDescriptions() + " " + task.getCreatedDate());

            // Card
            card = new Rectangle(200, 100 );
            if(Objects.equals(task.getStatus(), "1"))
                //gray
                card.setStyle("-fx-fill: #D3D3D3; -fx-stroke: #000000; -fx-stroke-width: 1;");
            else
                card.setStyle("-fx-fill: #FFFFFF; -fx-stroke: #000000; -fx-stroke-width: 1;");
            //card.setStyle("-fx-fill: #FFFFFF; -fx-stroke: #000000; -fx-stroke-width: 1;");

            this.getChildren().add(card);

            // Title
            Text titleText = new Text(task.getName());
            titleText.setStyle("-fx-font-size: 16px;");
            titleText.setTranslateY(-20);
            this.getChildren().add(titleText);

            // Date
            String date = String.valueOf(task.getDueDate());
            Text dateText;
            if(date == "null"){
                dateText = new Text("未設定截止日");
            }else {
                dateText = new Text(date);
            }
            dateText.setStyle("-fx-font-size: 12px;");
            dateText.setTranslateY(0);
            this.getChildren().add(dateText);

            // List
            Text categoryText = new Text(task.getListName());
            categoryText.setStyle("-fx-font-size: 12px;");
            categoryText.setTranslateY(20);
            this.getChildren().add(categoryText);

            this.task = task;

            // Click event
            this.setOnMouseClicked(e -> {
                passTaskInfo(index);
                currentTaskIndex = index;
            });
        }
    }


    /**
     * Here is the entry point for this view.
     * At here, we first prepare the data we need to display.
     * And do some settings for the view.
     * */
    @FXML protected HBox quickAdd;
    @FXML protected ChoiceBox list_selector;
    @FXML protected ChoiceBox quickZoneCateChooser;
    @FXML
    public void initialize() throws SQLException {

        // (for dropdown selector) load list and build name-id map
        ListNameIDMap = Main.db.fetchAllLists();
        reverseListNameIDMap = new HashMap<>();
        // build reverse map from id to name
        for(String key: ListNameIDMap.keySet()){
            reverseListNameIDMap.put(ListNameIDMap.get(key), key);
        }

        list_selector.getItems().clear();
        ListViewCateChooser.getItems().clear();
        quickZoneCateChooser.getItems().clear();
        list_selector.getItems().add("全部");
        for (String listName : ListNameIDMap.keySet()) {
            list_selector.getItems().add(listName);
            ListViewCateChooser.getItems().add(listName);
            quickZoneCateChooser.getItems().add(listName);
        }

        list_selector.getSelectionModel().select(0);

        // set quick zone default value
        quickZoneCateChooser.setValue(reverseListNameIDMap.get(Main.defaultListID));
        ////System.out.println(ListNameIDMap);

        loadTasks();

        //add radio buttons to the toggle group
        LV_p_critical.setToggleGroup(priorityGroup);
        LV_p_high.setToggleGroup(priorityGroup);
        LV_p_medium.setToggleGroup(priorityGroup);
        LV_p_low.setToggleGroup(priorityGroup);
        LV_p_unset.setToggleGroup(priorityGroup);


        quickAdd.setSpacing(15);

        if(!Main.loginStatus){
            SignBtn.setText("Sync With Google Task");
        }else{
            SignBtn.setText("Force Sync (Sync Every Lists)");
        }
    }

    @FXML protected HBox Board;


    @FXML
    protected void listSpecifiedList(){
        try{
            String selected = list_selector.getSelectionModel().getSelectedItem().toString();
            if(selected.equals("全部")){
                showSpecificList("all");
            }else{
                showSpecificList(ListNameIDMap.get(selected));
                //System.out.println(selected);
            }
        }catch (NullPointerException e){
            showSpecificList("all");
        }

    }

    @FXML
    protected void showSpecificList(String listID){
        //System.out.println("showing list: " + listID);
        //for card in taskContainer
        taskContainer.getChildren().clear();
        if(listID.equals("all")){
            for(Card card: cards){
                taskContainer.getChildren().add(card);
            }
        }else{
            for(Card card: cards){
                if(card.task.getListID().equals(listID)){
                    //show the card
                    taskContainer.getChildren().add(card);
                }else{
                    //System.out.println("not showing card: " + card.task.getName()+ " in list: " + card.task.getListID());
                }
            }
        }
        main_list_scroll_pane.setContent(taskContainer);
    }

    @FXML
    protected void renewCardList(){
        //System.out.println("Renewing");
        taskContainer.getChildren().clear();
        for(Card card: cards){
            taskContainer.getChildren().add(card);
        }
        main_list_scroll_pane.setContent(taskContainer);
    }

    @FXML private MenuItem SignBtn;
    @FXML
    protected void signBtnEvent() throws SQLException, IOException {
        if(!Main.loginStatus){
            Main.createClient();
            SignBtn.setText("Force Sync (Sync Every Lists)");
        }else{
            forceSync();
            renewHashMap(Main.db.fetchAllLists());
        }
    }

    /*--------------------------------------------------------------------------------
     * The following codes are controllers of the menu bar
     * From right to left, the first one is the "About" button, then "Edit", and "App".
     */

    /**
     * This method is used to open the License page
     */
    @FXML
    protected void openLicensePage() throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("views/license.fxml"));

        stage.setScene(new Scene(loader.load(), 640 , 480));
        //dont allow resizing
        stage.setResizable(false);
        stage.setTitle("License");
        stage.show();
    }

    /**
     * This method is used to open the repo page on user's default browser
     */
    @FXML
    protected void openRepoPage() {
        Main main = new Main();
        main.getHostServices().showDocument("https://gitlab.com/yen0224/112-2-csp-final");
    }

    @FXML
    protected void Sync() throws IOException, SQLException {
        showPromptBannerNoTimer("Syncing...");
        new Thread(() -> {
            Main.Sync();
            Platform.runLater(()->{
                showPromptBanner("Synced");
                try {
                    loadTasks();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });

        }).start();
    }

    @FXML
    protected void forceSync() throws IOException, SQLException {
        showPromptBannerNoTimer("Syncing All the list...");
        new Thread(() -> {
            try {
                Main.taskClient.startUpSync();
                Platform.runLater(()->{
                    showPromptBanner("Synced All the list");
                    try {
                        loadTasks();
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                });
            } catch (IOException | GeneralSecurityException e) {
                e.printStackTrace();
            }
        }).start();
    }

    @FXML
    protected void openSettingPage() throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("views/setting.fxml"));

        stage.setScene(new Scene(loader.load(), 320 , 240));
        //dont allow resizing
        stage.setResizable(false);
        stage.setTitle("Setting");
        stage.show();
    }

    @FXML
    protected void openCreatePage() throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("views/create.fxml"));

        stage.setScene(new Scene(loader.load(), 640 , 480));
        //dont allow resizing
        stage.setResizable(false);
        stage.setTitle("Create a new task");
        stage.show();
    }


    /**
     * There are two kinds of view mode, the "dashboard" and the "list".
     * In the dashboard mode, user can categories the tasks by their priority or any order they want.
     * In the list mode, user can see all the tasks in a list, and edit them by clicking on them.
     * */

    @FXML private ScrollPane main_list_scroll_pane; // scrollable container for the card's outer container
    @FXML private CheckBox showComplete;
    private VBox taskContainer = new VBox(); // container for the card

    /**
     * This function fetches all tasks stored in the db. and being call at the very beginning to prepare the data we need.
     * Once the loading process success, this function will create multiple cards to display different tasks, and put them into the container on the left,
     * also, while the card is creating, a counter will be used to memorize the original index of the task,
     * so that all other controller can use this index to fetch the task info in a short time (in order to avoid comparison).
     * */
    protected void loadTasks() throws SQLException {
        cards.clear();
        //fetch all tasks
        Tasks = Main.db.fetchAllTasks();
        //clear the list
        main_list_scroll_pane.setContent(null);
        //create a new VBox
        taskContainer.getChildren().clear();
        taskContainer.setSpacing(5);
        taskContainer.setAlignment(Pos.CENTER);

        int cardIndex = 0;
        //add cards to the VBox

        for (TaskClass task : Tasks) {
            Card card = new Card(task, cardIndex);
            taskContainer.getChildren().add(card);
            cards.add(card);
            cardIndex++;
        }
        //add the VBox to the scroll pane
        main_list_scroll_pane.setContent(taskContainer);
    }

    /**
     * The tasks can change at any time, so we need to update the list to make sure the data is up-to-date.
     * */
    protected void updateList() {
        // first check if the list is different
        try {
            TaskClass[] newTasks = Main.db.fetchAllTasks();
            if (newTasks.length != Tasks.length) {
                loadTasks();
            } else {
                for (int i = 0; i < newTasks.length; i++) {
                    if (!newTasks[i].equals(Tasks[i])) {
                        loadTasks();
                        break;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * The following codes are controllers of the task view on the right side.
     */
    @FXML private Text ListViewTaskTitle;
    @FXML private TextArea ListViewTaskDescription;
    @FXML private Text ListViewTaskCreateDate;
    @FXML private RadioButton LV_p_critical, LV_p_high, LV_p_medium, LV_p_low, LV_p_unset;
    private final ToggleGroup priorityGroup = new ToggleGroup();
    @FXML private DatePicker ListViewTaskDueDate;
    @FXML private ChoiceBox ListViewCateChooser;

    /**
     * @function priorityButtonReset
     * @usage reset the priority buttons
     */
    // for quick reset when switching tasks
    protected void priorityButtonReset(){
        LV_p_critical.setSelected(false);
        LV_p_high.setSelected(false);
        LV_p_medium.setSelected(false);
        LV_p_low.setSelected(false);
        LV_p_unset.setSelected(false);
    }

    /**
     * @function priorityButtonSet
     * @param priority: the priority of the task
     * @usage set the priority buttons
     */
    // for quick set when switching tasks
    protected void priorityButtonSet(int priority){
        priorityButtonReset();
        switch(priority){
            case 4 -> LV_p_critical.setSelected(true);
            case 3 -> LV_p_high.setSelected(true);
            case 2 -> LV_p_medium.setSelected(true);
            case 1 -> LV_p_low.setSelected(true);
            case 0 -> LV_p_unset.setSelected(true);
        }
    }

    // clear the task view
    @FXML
    protected void clearPreviousTask() {
        ListViewTaskTitle.setText("");
        ListViewTaskDescription.clear();
        ListViewTaskCreateDate.setText("");
        priorityButtonReset();
        ListViewTaskDueDate.setValue(null);
    }

    /**
     * @function passTaskInfo
     * @param index: the index of the task
     * @usage when card is clicked, the event handler will call this function to pass the task info into the task view
     */
    @FXML
    protected void passTaskInfo(int index){
        clearPreviousTask();
        enableEdit();
        TaskClass task = Tasks[index];

        ListViewTaskTitle.setText(task.getName());
        ListViewTaskDescription.setText(task.getDescriptions());
        ListViewTaskCreateDate.setText(task.getCreatedDate());

        ListViewCateChooser.setValue(task.getListName());
        priorityButtonSet(task.getPriority());
        if(task.getStatus().equals("1")){
            completeBtn.setText("未完成");
        }else{
            completeBtn.setText("完成");
        }
        if(task.getDueDate() != null){
            ListViewTaskDueDate.setValue(LocalDate.parse(task.getDueDate()));
        }else{
            ListViewTaskDueDate.setValue(null);
        }
    }

    @FXML private Button completeBtn;

    /**
     * @function updateTask
     * @usage update the task info
     */
    @FXML
    protected void updateTask() throws SQLException, IOException {
        if (currentTaskIndex == -1) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("No task selected");
            alert.setContentText("Please select a task to update");
            alert.showAndWait();
        } else {
            setButtonStatus();
            TaskClass task = Tasks[currentTaskIndex];
            String title = ListViewTaskTitle.getText();
            String description = ListViewTaskDescription.getText();
            int priority = 0;
            if (LV_p_critical.isSelected()) {
                priority = 4;
            } else if (LV_p_high.isSelected()) {
                priority = 3;
            } else if (LV_p_medium.isSelected()) {
                priority = 2;
            } else if (LV_p_low.isSelected()) {
                priority = 1;
            }
            String dueDate = String.valueOf(ListViewTaskDueDate.getValue());
            String toListID = ListNameIDMap.get(ListViewCateChooser.getValue().toString());

            task.setName(title);
            task.setDescriptions(description);
            task.setPriority(priority);
            task.setDueDate(dueDate);
            new Thread(() -> {
                Platform.runLater(()->{
                    showPromptBannerNoTimer("Updating task " + title + "...");
                });
                try {
                    Main.db.updateTask(task, task.getListID(), toListID);
                    Platform.runLater(()->{
                        showPromptBanner("Task " + title + " updated");
                    });
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }).start();

            //Main.db.updateTask(task);
            updateList();
            showPromptBanner("任務 " + title + " 已更新");
        }
    }

    /**
     * Bottom part: the quick zone to add a task
     */
    @FXML private TextField main_simple_title_tf;
    @FXML private DatePicker main_simple_date_field;
    @FXML private Button main_simple_create_btn;
    @FXML private Button main_simple_clear_btn;

    //main_list_scroll_pane
    @FXML
    protected void addTask() throws SQLException, IOException, InterruptedException {
        String title = main_simple_title_tf.getText();
        String listID = ListNameIDMap.get(quickZoneCateChooser.getValue().toString());
        String date;
        if(main_simple_date_field.getValue() != null){
            date = main_simple_date_field.getValue().toString();
        } else {
            date = null;
        }
        if (title.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("錯誤");
            alert.setHeaderText("請至少填入 '標題'");
            alert.setContentText("標題為必填欄位");
            alert.showAndWait();
        } else {
            showPromptBannerNoTimer("新增任務 " + title + " 中...");
            new Thread(()->{
                try {
                    Main.db.createTask(title, null, 0, listID, date);
                    clearFields();
                    showPromptBanner("任務 " + title + " 已新增");
                    updateList();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }).start();
            }
        }


    /**
     * Utility function to clear the fields
     * */
    @FXML
    protected void clearFields() {
        main_simple_title_tf.clear();
        main_simple_date_field.setValue(null);
    }

    /**
     * Utility function to check the fields
     */
    @FXML
    protected void checkFields() {
        //clear button
        // if one of the fields is not empty, enable the clear button
        main_simple_clear_btn.setDisable(main_simple_title_tf.getText().isEmpty() && main_simple_date_field.getValue() == null);

        //create button
        // if both fields are not empty, enable the create button
        main_simple_create_btn.setDisable(main_simple_title_tf.getText().isEmpty());
    }

    /**
     * Prompt banner
     */

    @FXML private Text main_simple_prompt_text;
    /** any controller can call this function to show a prompt banner for 3 seconds */
    protected void showPromptBanner(String msg) {
        main_simple_prompt_text.setText(msg);
        main_simple_prompt_text.setVisible(true);
            //hide after 3 seconds
            new Timer().schedule(
                    new TimerTask() {
                        @Override
                        public void run() {
                            main_simple_prompt_text.setVisible(false);
                        }
                    },
                    3000
            );
    }

    protected void showPromptBannerNoTimer(String msg) {
        main_simple_prompt_text.setText(msg);
        main_simple_prompt_text.setVisible(true);
    }

    protected void hidePromptBanner() {
        main_simple_prompt_text.setVisible(false);
    }

    @FXML private Button taskUpdateBtn;

    @FXML protected void enableEdit(){
        ListViewTaskTitle.setDisable(false);
        ListViewTaskDescription.setDisable(false);
        ListViewTaskDueDate.setDisable(false);
        LV_p_critical.setDisable(false);
        LV_p_high.setDisable(false);
        LV_p_medium.setDisable(false);
        LV_p_low.setDisable(false);
        LV_p_unset.setDisable(false);
        completeBtn.setDisable(false);
        taskUpdateBtn.setDisable(false);
        ListViewCateChooser.setDisable(false);
    }

    @FXML
    protected void updateTaskStatus() throws SQLException, IOException {
        String status = Tasks[currentTaskIndex].getStatus();
        //System.out.println(status);
        // 1 is finished, 0 is unfinished
        new Thread(()->{
            if(status.equals("1")){
                try {
                    Tasks[currentTaskIndex].unfinishedTask();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                completeBtn.setText("完成");

            }else{
                try {
                    Tasks[currentTaskIndex].finishTask();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                completeBtn.setText("未完成");
            }
            try {
                Sync();
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }).start();

    }

    protected void setButtonStatus(){
        String status = Tasks[currentTaskIndex].getStatus();
        if(status.equals("1")){
            completeBtn.setText("未完成");
        }else{
            completeBtn.setText("完成");
        }
    }

    @FXML
    protected void quit(){
        System.exit(0);
    }

    public void renewHashMap(Map<String, String> newMap) throws SQLException {
        ListNameIDMap = newMap;
        reverseListNameIDMap = new HashMap<>();
        // build reverse map from id to name
        for(String key: ListNameIDMap.keySet()){
            reverseListNameIDMap.put(ListNameIDMap.get(key), key);
        }

        //clear previous map
        ListViewCateChooser.getItems().clear();
        quickZoneCateChooser.getItems().clear();
        list_selector.getItems().clear();

        list_selector.getItems().add("全部");
        for (String listName : ListNameIDMap.keySet()) {
            list_selector.getItems().add(listName);
            ListViewCateChooser.getItems().add(listName);
            quickZoneCateChooser.getItems().add(listName);
        }
    }
}